
from flask import Flask,jsonify
from flask_sse import sse
import logging
from apscheduler.schedulers.background import BackgroundScheduler
from flask_cors import CORS
import datetime
import uuid
import random
from faker import Faker
app = Flask(__name__)
CORS(app)
app.config["REDIS_URL"]="redis://172.17.0.2"
app.register_blueprint(sse, url_prefix='/events')

fake = Faker()

def get_data():
    data = list()
    data.append({'name': fake.name()})
    return data



def server_side_event():
    """ Function to publish server side event """

    with app.app_context():
        sse.publish(get_data(), type='customer')
        print("New Customer Time: ",datetime.datetime.now())
   


sched = BackgroundScheduler(daemon=True)
sched.add_job(server_side_event,'interval',seconds=10, id ="myJob")
sched.start()


